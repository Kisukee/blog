---
title: Third-party Services
description: NexT User Docs – Third-party Service Integration
---
Static website is limited in some functions, so we need third-party services to extend our site.
In any time you can extend needed functions by using third-party services supported by NexT.

### Comment Systems

* [Disqus](./comments-and-widgets/#Disqus)
* [Facebook Comments](./comments-and-widgets/#Facebook-Comments)
* [LiveRe](./comments-and-widgets/#LiveRe)
* [Gitment](./comments-and-widgets/#Gitment)
* [Changyan](./comments-and-widgets/#Changyan)
* [Valine](./comments-and-widgets/#Valine)
* [VKontakte Comments and Likes](./comments-and-widgets/#VKontakte-Comments-and-Likes)
* [Widgetpack Rating](./comments-and-widgets/#Widgetpack-Rating)

{% note info %}
If you want to disable comment in some page/post, just add `comments: false` to {% exturl front-matter https://hexo.io/docs/front-matter.html %} in your markdown file.
{% endnote %}

### Statistics and Analytics

* [Google Webmaster Tools](./statistics-and-analytics/#Google-Webmaster-Tools)
* [Bing Webmaster Tools](./statistics-and-analytics/#Bing-Webmaster-Tools)
* [Yandex Webmaster Tools](./statistics-and-analytics/#Yandex-Webmaster-Tools)
* [Baidu Webmaster Tools](./statistics-and-analytics/#Baidu-Webmaster-Tools)
* [Google Analytics](./statistics-and-analytics/#Google-Analytics)
* [Azure Application Insights](./statistics-and-analytics/#Azure-Application-Insights)
* [Baidu Analytics (China)](./statistics-and-analytics/#Baidu-Analytics-China)
* [Tencent Analytics (China)](./statistics-and-analytics/#Tencent-Analytics-China)
* [CNZZ Analytics (China)](./statistics-and-analytics/#CNZZ-Analytics-China)
* [Tencent Mobile Analytics (China)](./statistics-and-analytics/#Tencent-Mobile-Analytics-China)
* [LeanCloud (China)](./statistics-and-analytics/#LeanCloud-China)
* [Busuanzi Counting (China)](./statistics-and-analytics/#Busuanzi-Counting-China)
* [Firebase](./statistics-and-analytics/#Firebase)

### Content Sharing Services

* [AddThis](./content-sharing-services/#AddThis)
* [Need More Share](./content-sharing-services/#Need-More-Share)
* [Baidu Sharing (China)](./content-sharing-services/#Baidu-Sharing-China)

### Search Services

* [Local Search](./search-services/#Local-Search)
* [Algolia Search](./search-services/#Algolia-Search)
* [Swiftype](./search-services/#Swiftype-Search)

### External Libraries

* [Fancybox](./external-libraries/#Fancybox)
* [Bookmark](./external-libraries/#Bookmark)
* [Reading Progress](./external-libraries/#Reading-Progress)
* [Progress bar](./external-libraries/#Progress-bar)
* [FastClick](./external-libraries/#FastClick)
* [Jquery Lazyload](./external-libraries/#Jquery-Lazyload)
* [Backgroud JS](./external-libraries/#Backgroud-JS)
* [Han Support](./external-libraries/#Han-Support)
* [Pangu Autospace Support](./external-libraries/#Pangu-Autospace-Support)

### Math Equations

* [Math Equations](./math-equations/)
